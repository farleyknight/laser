module Laser
  module Analysis
    module IncorrectPredicateMethodDetection
      # Usage: Decide if methods ending with '?' return either
      # true, false or nil.
      def incorrect_predicate_methods
        each_user_method.select do |method|
          method.name.end_with?('?')
        end.select do |method|
          !correct_predicate_type?(method.combined_return_type)
        end
      end

      # Usage: Three checks against subtype
      # 1. Check if it's a subtype of the False class
      # 2. Check if it's a subtype of the NilClass
      # 3. Check if it's a subtype of the Union (True || False)
      def correct_predicate_type?(return_type)
          (Types.subtype?(Types::FALSECLASS, return_type) ||
          Types.subtype?(Types::NILCLASS, return_type)) &&
          !Types.subtype?(return_type, Types::FALSY)
      end
    end
  end
end
